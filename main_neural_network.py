#--------------------------------------------------------------------
#EM 17/09/2021
#A Deep Neural Network (NN) for Image Classification
#to recognise cat and non-cat pictures
#Builds a L-layer Neural Network
#Plots costs against iterations
#Predicts the accuracy of train and test sets
#Tests the classification of my own image
#Plots the misclassified images of test set
#Modify input section for the desired NN model
#--------------------------------------------------------------------
import matplotlib.pyplot as plt
import class_data as dt
import numpy as np
import matplotlib.pyplot as plt
from class_color import bcolors as bc
from functions_essentls import *
from functions_plots import *


### INPUT SECTION STARTS HERE------------------------------------------
#Learning rate
lrate  = 0.0075
n_iter = 2500
num_px = 64
#Define layers (NLs) and the number of units in each layers
#ex. of a 4-layer  
NL1    = 20
NL2    = 7
NL3    = 5
NL4    = 1
#If number of NL changes, the below layer_dims should be modified accordingly.

# INPUT SECTION END------------------------------------------------------

data=dt.dataset()
train_x, train_y,test_x, test_y,classes=data.pic_prprtion(print_info=True)
Nx = np.shape(train_x)[0] 
layers_dims = [Nx, NL1, NL2, NL3, NL4]

print (bc.BOLD+"\n Deep Neural Network for Image Classification")
print (" to recognise cat and non-cat pictures")
print ("========================================="+bc.ENDC)
print ("The number of layers are: "+str(len(layers_dims)-1))
print ("  ")
print (bc.BOLD+"Building a "+str(len(layers_dims)-1)+\
       "-layer Neural Network ..."+bc.ENDC)

parameters, costs = L_layer_model(train_x, train_y,\
                                  layers_dims,\
                                  learning_rate = lrate,\
                                  num_iterations = n_iter ,\
                                  print_cost = True)

print ("  ")
print (bc.BOLD+"Plotting costs against iterations ..."+bc.ENDC)

plot_costs(costs, lrate)
#---------------------------------------------------------------------------
print ("  ")
print (bc.BOLD+"Predicting the accuracy of train and test sets ..."+bc.ENDC)

pred_train, accuracy = predict(train_x, train_y, parameters)
#---------------------------------------------------------------------------
print ('Accuracy of train set:', accuracy)

pred_test, accuracy = predict(test_x, test_y, parameters)

print('Accuracy of test set:', accuracy)
#----------------------------------------------------------------------------
print ("  ")
print (bc.BOLD+"Testing my image ..."+bc.ENDC)

my_image = "my_image.jpg"
test_image(my_image,num_px,parameters,classes)
#-----------------------------------------------------------------------------
print ("  ")
print (bc.BOLD+"Plotting mislabeled images ..."+bc.ENDC)

print_mislabeled_images(classes, test_x, test_y, pred_test)

plt.show()
#----------------------------------------------------------------------------
print ("  ")
print (bc.BOLD+"Program Done!"+bc.ENDC)
print (" ")

