import matplotlib.pyplot as plt
from functions_essentls import *
from PIL import Image

def print_mislabeled_images(classes, X, y, p):
    """
    Plots images where predictions and truth were different.
    X -- dataset
    y -- true labels
    p -- predictions
    """
    a = p + y
    mislabeled_indices = np.asarray(np.where(a == 1))
    plt.rcParams['figure.figsize'] = (20.0, 10.0) # set default size of plots
    num_images = len(mislabeled_indices[0])
    m_column=5
    m_raw = int(num_images/m_column)+ num_images%m_column
    for i in range(num_images):
        index = mislabeled_indices[1][i]
        
        plt.subplot(m_raw, m_column, i + 1)
        plt.imshow(X[:,index].reshape(64,64,3), interpolation='nearest')
        plt.axis('off')
        plt.title("Prediction: " + classes[int(p[0,index])].decode("utf-8") +\
                  " \n Class: " + classes[y[0,index]].decode("utf-8"),\
                  fontsize=20)



def plot_costs(costs, learning_rate):
    plt.plot(np.squeeze(costs))
    plt.ylabel('cost')
    plt.xlabel('iterations (per hundreds)')
    plt.title("Learning rate =" + str(learning_rate))
    plt.show()


def test_image(my_image,num_px,parameters,classes):
    # the true class of your image (1 -> cat, 0 -> non-cat)
    my_label_y = [1]
    fname = "./images/" + my_image
    image = np.array(Image.open(fname).resize((num_px, num_px)))
    plt.imshow(image)
    plt.title("My test image")
    plt.show()
    image = image / 255.
    image = image.reshape((1, num_px * num_px * 3)).T

    my_predicted_image,_ = predict(image, my_label_y, parameters)

    print ("y = " + str(np.squeeze(my_predicted_image)) +\
           ", your L-layer model predicts a \"" + \
           classes[int(np.squeeze(my_predicted_image)),].decode("utf-8") +\
           "\" picture.")

